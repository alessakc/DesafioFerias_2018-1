<?php
    
    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
    
    class DatabaseSeeder extends Seeder {
        public function run() {
            Model::unguard();
    
            App\Role::where('id', '>', '2')->delete();
            App\User::where('id', '>', '1')->forceDelete();

            //Chamada de todos as classes de seeder
            $this->call(EjTableSeeder::class);
            $this->call(RoleAndUserTableSeeder::class);
            $this->call(GroupCategorySeeder::class);
            $this->call(GroupSeeder::class);

            Model::reguard();
    
        }
    }
