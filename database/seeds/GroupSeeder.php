<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    private function getRandGroups($groups){
        $groupsId = $groups->pluck('id')->toArray();
        $rand = array_rand($groupsId, rand(1, 3));
        if(!is_array($rand)){
            $rand = [$rand];
        }
        $randNumbers = [];
        foreach($rand as $value){
            $randNumbers[] = $groupsId[$value];
        }
        return $randNumbers;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = factory(App\Group::class, 10)->create();
        $users = App\User::get();
        foreach($users as $user){
            $user->groups()->sync($this->getRandGroups($groups));
        }
    }
}
