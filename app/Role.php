<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['access_level', 'authorization', 'name'];

    /**
     * Relationships 
    */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function permissions()
    {
        return $this->hasMany('App\Permission');
    }

    public function indicators()
    {
        return $this->hasMany('App\Indicator');
    }
    
    
    /*
     * Query scopes 
    */
    
    public function scopeGuest($query)
    {
        return $query->where('name', '=', 'guest')->first();
    }
    
    public function scopeNoGuest($query)
    {
        return $query->where('name', '!=', 'guest')->get();
    }
    
    /**
     * Assessors
     */
    public function getRouteListAttribute()
    {
        return $this->permissions->pluck('route')->toArray();
    }
}
