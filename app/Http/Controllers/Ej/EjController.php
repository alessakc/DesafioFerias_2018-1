<?php

namespace App\Http\Controllers\Ej;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ej;
use App\Http\Requests\EjRequest;
use App\Http\Requests\UpdateEjRequest;

class EjController extends Controller
{
    public function index() 
    {
        $ejs = Ej::get();
        return view('ej.index')->with('ejs', $ejs);
    }
    
    public function show($id)
    {
        $ej = Ej::findOrFail($id);
        return view('ej.show')->with('ej', $ej);
    }

    public function create() 
    {
        return view('ej.register');
    }
    
    public function store(EjRequest $request)
    {
        $ej = new Ej;
        $ej->name = $request->input('name');
        $ej->about = $request->input('about');
        $ej->area = $request->input('area');
        $ej->site = $request->input('site');
        $ej->email = $request->input('email');
        $ej->state = $request->input('state');
        $ej->city = $request->input('city');
        $ej->address = $request->input('address');
        $ej->university = $request->input('university');
        $ej->majors = $request->input('majors');
        $ej->facebook = $request->input('facebook');
        $ej->phone = $request->input('phone');
        $ej->save();
		if ($image = $request->file('ej_image')) {
		    if(($image->isValid()? substr($image->getMimeType(), 0, 5): null) == 'image') {
    			$destinationPath = base_path() . '/public/img/ejs/';
    			$ej->image = $ej->id . '.' . $image->getClientOriginalExtension();
    			$image->move($destinationPath, $ej->image);
            	$ej->save();
		    }
		}
        return redirect('/ejs');
    }
    
    public function edit($id)
    {
        $ej = Ej::findOrFail($id);
        return view('ej.edit')->with('ej', $ej);
    }
    
    public function update(UpdateEjRequest $request, $id)
    {   
        $ej = Ej::findOrFail($id);
        $ej->name = $request->input('name');
        $ej->about = $request->input('about');
        $ej->area = $request->input('area');
        $ej->site = $request->input('site');
        $ej->email = $request->input('email');
        $ej->state = $request->input('state');
        $ej->city = $request->input('city');
        $ej->address = $request->input('address');
        $ej->university = $request->input('university');
        $ej->majors = $request->input('majors');
        $ej->facebook = $request->input('facebook');
        $ej->phone = $request->input('phone');
		if ($image = $request->file('ej_image')) {
		    if(($image->isValid()? substr($image->getMimeType(), 0, 5): null) == 'image') {
        		$imagePath = base_path() . '/public/img/ejs/' . $ej->image;
        		if(file_exists($imagePath)) {
        			unlink($imagePath);
        		}
    			$destinationPath = base_path() . '/public/img/ejs/';
    			$ej->image = $ej->id . '.' . $image->getClientOriginalExtension();
    			$image->move($destinationPath, $ej->image);
		    }
		}
        $ej->update();
        return redirect('/ejs/' . $id);
    }
    
    public function destroy($id)
    {
        $ej = Ej::findOrFail($id);
		$imagePath = base_path() . '/public/img/ejs/' . $ej->image;
		if(file_exists($imagePath)) {
			@unlink($imagePath);
		}
        $ej->delete();
        return redirect('/ejs');
    }
}
