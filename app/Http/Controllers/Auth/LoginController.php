<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/auth/profile';
     protected $redirectAfterLogout = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        $user = Auth::user();
        if ($user->first_login) {
          return redirect()->intended('auth/profile/');
        }
        else {
          $user->first_login = 1;
          $user->save();
          return redirect('/password/first/reset/' . $user->remember_token)->with('user', $user);
        }
      }

      return redirect('/login')->with('status', 'Falha na tentative de login! Email ou senha incorretos!');
    }
}
