<?php

namespace App\Http\Controllers\Group;

use Illuminate\Http\Request;
use App\GroupCategory;
use App\Http\Requests;
use App\Http\Requests\GroupCategoryRequest;
use App\Http\Controllers\Controller;

class GroupCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_categories = GroupCategory::all();
        return view('config_panel.group.group_category.index', compact('group_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupCategoryRequest $request)
    {
        GroupCategory::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group_category = GroupCategory::findOrFail($id);
        return view('config_panel.group.group_category.edit', compact('group_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupCategoryRequest $request, $id)
    {
        $group_category = GroupCategory::FindOrFail($id);
        $group_category->update($request->all());
        return redirect('config/groups/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GroupCategory::destroy($id);
        return redirect()->back();
    }
}
