<?php
use Carbon\Carbon;

function hasPermission($route)
{
    if (\App::environment('production', 'production_show_errors'))
    {
        if(!\Auth::check()){
            return App\Role::where('name', 'guest')->first()->permissions->contains('route', $route);
        } else {
            foreach(\Auth::user()->roles as $role){
                if($role->permissions->contains('route', $route)){
                    return true;
                }
            }
            return false;
        }
    }
    return true;
}

function getNextIndicatorFill($indicator)
{
    if($indicator->fills->isEmpty())
        return Carbon::createFromFormat('d/m/Y', $indicator->fill_start);
    $last_fill = Carbon::createFromFormat('d/m/Y', $indicator->fills->max('filled_at'));
    return $indicator->interval_days == 30 ? $last_fill->addMonths(1) : $last_fill->addDays($indicator->interval_days);
}

function getNextIndicatorGoal($indicator)
{
    if($indicator->goals->isEmpty())
        return Carbon::createFromFormat('d/m/Y', $indicator->fill_start);
    $last_fill = Carbon::createFromFormat('d/m/Y', $indicator->goals->max('goal_date'));
    return $indicator->interval_days == 30 ? $last_fill->addMonths(1) : $last_fill->addDays($indicator->interval_days);
}

