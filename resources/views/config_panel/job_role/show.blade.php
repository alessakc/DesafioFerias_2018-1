@extends('config_panel.layout')

@section('plugins-top')
  <!-- Bootstrap table css -->
  <link href="{{ asset('dashboard/plugins/bootstrap-table/css/bootstrap-table.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('dashboard/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"  />

  <!-- Sweet alert -->
  <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
@if (session()->has('flash_message'))
    <div class="row" style="text-align: center;">
        <div class="col-lg-12">
            <div class="panel panel-border panel-{{ session('flash_message_level') }}">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ session('flash_message_title') }}</h3>
                </div>
                <div class="panel-body" style="text-align: center;">
                    <p>
                      {{ session('flash_message') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
            <h4 class="page-title">Cargos</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Midas</a>
                </li>
                <li>
                    <a href="/config">Painel de controle</a>
                </li>
                <li>
                    <a href="/config/job_roles">Gerenciar cargos</a>
                </li>
                <li class="active">
                    Cargos
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="panel-header">
        <h4 class="m-t-0 header-title"><b>Editar cargo</b></h4>
      </div>
      <div class="panel-body">
        <div class="row form-horizontal">
          {!! Form::open(['id' => 'editjobf-' . $job_role->id, 'url' => '/config/job_roles/' . $job_role->id, 'method' => 'PUT']) !!}
            <div class="form-group">
              {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
              <div class="col-sm-5">
                  {!! Form::text('name', $job_role->name , ['class' => 'form-control col-md-4', 'Placeholder' => 'Nome do cargo']) !!}
                  @if ($errors && $errors->has('name'))
                    <span class="help-block">
                        <strong style="color:red;">{{ $errors->first('name') }}</strong>
                    </span>
                  @endif
              </div>
            </div>
            <div class="text-center">
                <a href="{{ url('/config/job_roles') }}" class="btn waves-effect waves-light btn-default m-r-15">Voltar</a>
                <button class='btn waves-effect waves-light btn-default' id="editjob-{{$job_role->id}}">Editar</button>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="panel-header">
        <h4 class="m-t-0 header-title"><b>Lista de cargos</b></h4>
      </div>
      <div class="panel-body">
        <div class="col-sm-12">
          <table id="custom-table"  data-toggle="table"
             data-search="true"
             data-show-toggle="true"
             data-show-columns="true"
             data-sort-order="desc"
             data-page-list="[5, 10, 20]"
             data-page-size="5"
             data-pagination="true"
             data-show-pagination-switch="true"
             data-locale="pt-BR"
             class="table-bordered ">
             <thead>
                <tr>
                  <th data-field="status" data-align="center" data-sortable="true">Nome</th>
                  <th data-field="type" data-align="center" data-sortable="true">Grupos</th>
                </tr>
            </thead>
            @foreach ($users as $user)
              @if ($user->groups()->wherePivot('job_role_id', $job_role->id)->get()->all())
                <tr>
                  <td align="center" valign="middle">{{ $user->name }}</td>
                  <td>
                    <select id="select-{{ $user->id }}" class="form-control input-md">
                      <option value="" default>Lista de grupos</option>
                      @foreach ($user->groups()->wherePivot('job_role_id', $job_role->id)->get()->all() as $group)
                        <option value="{{ $user->id }}-{{ $group->id }}">
                            {{ $group->name }}
                        </option>
                      @endforeach
                    </select>
                  </td>
                </tr>
              @endif
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('plugins-scripts')
  <script>
    $(document).ready(function() {
      $('#editjob-{{$job_role->id}}').click(function(){
        event.preventDefault();
        swal({
            title: "Tem certeza que deseja editar o cargo selecionado?",
            text: "Os usuários que possuem o cargo receberão essa modificação!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-warning',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sim, editar!",
            closeOnConfirm: true,
            allowOutsideClick: true,
        },
        function() {
          $('#editjobf-{{$job_role->id}}').submit();
        });
      });
    });
  </script>

  <!-- Sweet alert -->
  <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

  <!-- Bootstrap plugin js -->
  <script src="{{ asset('dashboard/plugins/bootstrap-table/js/bootstrap-table.min.js') }}"></script>
  <script src="{{ asset('dashboard/pages/jquery.bs-table.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-table/locale/bootstrap-table-pt-BR.js') }}"></script>
@endsection
