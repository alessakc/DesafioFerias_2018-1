@extends('config_panel.layout')

@section('css')
    <link href="{{ asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('plugins-top')
    <link href="{{ asset('dashboard/css/dataTables.colVis.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
            <h4 class="page-title">Grupos</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Midas</a>
                </li>
                <li class="active">
                    Grupos
                </li>
            </ol>
        </div>
        <div class="btn-group pull-right m-t-15">
            <a type="button" class="btn btn-default waves-effect waves-light" aria-expanded="false" href="/config/groups/category"><i class="md md-add"></i><span class="m-l-5">Ver Categorias de Grupo</span></a>
        </div>
    </div>
</div>

@if (hasPermission('group.store'))
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Cadastrar novo grupo</b></h4>
                <div class="row form-horizontal">
                    {!! Form::open() !!}
                    @include('config_panel.group.form', ['text' => "Deixe em branco para gerar automaticamente e enviar por email"])
                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-4 col-sm-3">
                        {!! Form::button('Cadastrar', ['type'=>'submit', 'class' => 'btn waves-effect waves-light btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 m-b-30 header-title"><b>Lista de grupos</b></h4>
            <table id="datatable-colvid" class="table table-striped table-bordered table-actions-bar">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Membros</th>
                        <th>Editar</th>
                        <th>Apagar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($groups as $group)
                    <tr>
                        <td>{{ $group->name }}</td>
                        <td>{{ $group->category->name }}</td>
                        <td>
                            {{ $group->users->implode('name', ', ') }}
                        </td>
                        <td align="center">
                            @if (hasPermission('group.edit'))
                                {!! Form::open(['route' => ['config.groups.edit', $group->id], 'method' => 'get']) !!}
                                {!! Form::button('<i class="md md-edit"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                        <td align="center">
                            @if (hasPermission('group.destroy'))
                                {!! Form::open(['route' => ['config.groups.destroy', $group->id], 'method' => 'delete', 'id' => 'delete' . $group->id]) !!}
                                {!! Form::button('<i class="md md-close"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('plugins-scripts')

    <script type="text/javascript" src="{{ asset('dashboard/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.colVis.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({keys: true});
            $('#datatable-responsive').DataTable();
            $('#datatable-colvid').DataTable({
                "dom": 'C<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Change columns"
                }
            });
            $('#datatable-scroller').DataTable({
                ajax: "assets/plugins/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            var table = $('#datatable-fixed-col').DataTable({
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns: 1
                }
            });
        });
        TableManageButtons.init();
    </script>
    <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/pages/jquery.sweet-alert.init.js')}}" type="text/javascript"></script>

    <script src="{{ asset('dashboard/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('dashboard/pages/jquery.form-pickers.init.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/pages/jquery.form-advanced.init.js') }}"></script>
@foreach ($groups as $group)
    <script>
      $('#delete{{$group->id}}').click(function(event) {
          event.preventDefault();
          swal({
            title: "Tem certeza que deseja deletar o usuário {!! $group->name !!}?",
            text: "O usuário será apagado permanentemente do sistema e não poderá ser recuperado.",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sim, deletar!",
            closeOnConfirm: false,
            allowOutsideClick: true,
          },
          function(){
            $('#delete{{$group->id}}').submit();
          });
      });
    </script>
@endforeach

@endsection
