@extends('config_panel.layout')

@section('css')
    <link href="{{ asset('dashboard/css/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/multi-select.css')}}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('plugins-top')

    <link href="{{ asset('dashboard/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/dataTables.colVis.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/fixedColumns.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@stop

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Membros</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li class="active">
                Membros
            </li>
        </ol>
    </div>
</div>

   <!-- início form -->

@if (hasPermission('user.store'))
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Cadastrar novo membro</b></h4>
                <div class="row form-horizontal">
                    {!! Form::open(['enctype' => 'multipart/form-data']) !!}
                    @include('config_panel.user.form', ['text' => "Deixe em branco para gerar automaticamente e enviar por email"])
                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-4 col-sm-3">
                        {!! Form::button('Cadastrar', ['type'=>'submit', 'class' => 'btn waves-effect waves-light btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif

<!-- fim form -->
<!-- início da exibição de membros !-->
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 m-b-30 header-title"><b>Lista de membros</b></h4>
            <table id="datatable-colvid" class="table table-striped table-bordered table-actions-bar">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Usuário</th>
                        <th>Email</th>
                        <th>Data de nascimento</th>
                        <th>Data em que entrou na empresa</th>
                        <th>Editar</th>
                        <th>Apagar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }} {{ $user->lastname }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->birthday }}</td>
                        <td>{{ $user->join_date }}</td>
                        <td align="center">
                            @if (hasPermission('user.edit'))
                                {!! Form::open(['route' => ['config.users.edit', $user->id], 'method' => 'get']) !!}
                                {!! Form::button('<i class="md md-edit"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                        <td align="center">
                            @if (hasPermission('user.destroy'))
                                {!! Form::open(['route' => ['config.users.destroy', $user->id], 'method' => 'delete', 'id' => 'delete' . $user->id]) !!}
                                {!! Form::button('<i class="md md-close"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    <!-- final da exibição dos membros !-->

    <!-- Fim do novo formulário !-->

@endsection

@section('plugins-scripts')

    <script src="{{ asset('dashboard/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/jszip.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/pdfmake.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/vfs_fonts.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/responsive.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.colVis.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.fixedColumns.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/datatables.init.js')}}"></script>

        <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({keys: true});
            $('#datatable-responsive').DataTable();
            $('#datatable-colvid').DataTable({
                "dom": 'C<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Change columns"
                }
            });
            $('#datatable-scroller').DataTable({
                ajax: "assets/plugins/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            var table = $('#datatable-fixed-col').DataTable({
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns: 1
                }
            });
        });
        TableManageButtons.init();

    </script>

    <script src="{{ asset('dashboard/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/jquery.mockjax.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/countries.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/pages/autocomplete.js') }}"></script>

    <script src="{{ asset('dashboard/plugins/moment/moment.js') }}"></script>
    <!--Ubold Pickers-->
    <script src="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('dashboard/pages/jquery.form-pickers.init.js') }}"></script>
    <!-- Forma temporária de deixar o daterangepicker em ptbr -->
    <script src="{{ asset('js/datapickerpt-br.js')}}"></script>

    <!--Filestyle-->
    <script src="{{ asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
 


    <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/pages/jquery.sweet-alert.init.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('dashboard/pages/jquery.form-advanced.init.js') }}"></script>
@foreach ($users as $user)
    <script>
      $('#delete{{$user->id}}').click(function(event) {
          event.preventDefault();
          swal({
            title: "Tem certeza que deseja deletar o usuário {!! $user->name !!}?",
            text: "O usuário será apagado permanentemente do sistema e não poderá ser recuperado.",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sim, deletar!",
            closeOnConfirm: false,
            allowOutsideClick: true,
          },
          function(){
            $('#delete{{$user->id}}').submit();
          });
      });
    </script>
@endforeach
@endsection
