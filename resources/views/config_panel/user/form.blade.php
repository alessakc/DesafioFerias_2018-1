<div class="form-group">
    {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('name', null, ['class' => 'form-control', 'Placeholder' => 'Nome']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('lastname', 'Sobrenome:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('lastname', null, ['class' => 'form-control', 'Placeholder' => 'Sobrenome']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('username', 'Usuário:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('username', null, ['class' => 'form-control', 'Placeholder' => 'Usuário']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('email', null, ['class' => 'form-control', 'Placeholder' => 'Email']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('facebook', null, ['class' => 'form-control', 'Placeholder' => 'Facebook']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('password_hack', 'Senha:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::password('password_hack', ['class' => 'form-control', 'Placeholder' => $text]) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('birthday', 'Data de nascimento:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
        <div class="input-group">
        	{!! Form::text('birthday', null, ['class' => 'form-control date datepicker-pt-br', 'Placeholder' => 'dd/mm/aaaa', 'id' => 'datepicker-autoclose', 'data-mask' => '99/99/9999']) !!}
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
        </div>
	</div>
</div>
<div class="form-group">
    {!! Form::label('join_date', 'Data de entrada:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
        <div class="input-group">
        	{!! Form::text('join_date', null, ['class' => 'form-control date datepicker-pt-br', 'Placeholder' => 'dd/mm/aaaa', 'id' => 'datepicker-autoclose', 'data-mask' => '99/99/9999']) !!}
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
        </div>
	</div>
</div>
<div class="form-group">
    {!! Form::label('exit_date', 'Data de saída:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
        <div class="input-group">
        	{!! Form::text('exit_date', null, ['class' => 'form-control date datepicker-pt-br', 'Placeholder' => 'dd/mm/aaaa', 'id' => 'datepicker-autoclose', 'data-mask' => '99/99/9999']) !!}
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
        </div>
	</div>
</div>
<div class="form-group">
    {!! Form::label('image', 'Foto', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
        <input class="filestyle" data-buttonname="btn-primary" data-buttonText="Escolher Imagem" type="file" name="image">
    </div>
</div>
{{-- <!--<div class="form-group">
    {!! Form::label('role_list', 'Cargos:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::select('role_list[]', $roles, null, ['class' => 'select2 select2-multiple', 'multiple', 'title' => 'Cargos (opcional)']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('group_list', 'Grupos:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
        {!! Form::select('group_list[]', $groups, null, ['class' => 'select2 select2-multiple', 'multiple', 'title' => 'Grupos']) !!}
    </div>
</div>
<div class=form-group>
    {!! Form::label('ej_id', 'Empresa Júnior:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
        {!! Form::select('ej_id', [NULL => 'Nenhuma empresa júnior'] + $ejs, null, ['class' => 'form-control', 'title' => 'Qual Empresa Júnior empresa foi contradada pelo cliente']) !!}
    </div>    
</div>--> --}}
@include('errors.validator')