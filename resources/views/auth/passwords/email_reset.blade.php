@extends('auth.alayout')

@section('content')
  <div class=" card-box">
  	<div class="panel-heading">
  		<h3 class="text-center"> Editar <strong class="text-custom">senha</strong> </h3>
  	</div>
  	<div class="panel-body">
  	<form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('password/email/reset/' . $token) }}" enctype="multipart/form-data">
  		{{ csrf_field() }}
  		<div class="form-group ">
  			<div class="col-xs-12">
  				<strong>Senha nova:</strong><input class="form-control" name="password_new" value="" id="password_new" type="password">
  				@if ($errors && $errors->has('password_new'))
  					<span class="help-block">
  							<strong style="color:red;">{{ $errors->first('password_new') }}</strong>
  					</span>
  				@endif
  			</div>
  		</div>
  		<div class="form-group ">
  			<div class="col-xs-12">
  				<strong>Confirmação:</strong><input class="form-control" name="password_new_confirmation" value="" id="password_new_confirmation" type="password">
  				@if ($errors && $errors->has('password_new_confirmation'))
  					<span class="help-block">
  							<strong style="color:red;">{{ $errors->first('password_new_confirmation') }}</strong>
  					</span>
  				@endif
  			</div>
  		</div>
  		<div class="form-group text-center m-t-40">
  			<div class="col-xs-12">
  				<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
  					Enviar
  				</button>
  			</div>
  		</div>
  	</form>
  	</div>
  </div>
@stop
